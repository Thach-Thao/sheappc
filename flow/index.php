<?php $id="flow";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-flow l-container-fluid01">
	<div class="l-wrapper">
		<div class="c-breadcrumb">
			<ul>
				<li><a href="">トップページ＞</a></li>
				<li>住まいづくりのすすめ方</kli>
			</ul>
		</div>
		<div class="c-title01">
			F<span class="c-title01--color2">l</span>ow
			<span class="c-title01--smtxt c-title01--color2"> 住まいづくりのすすめ方</span>
		</div>

		<div class="p-flow01">
			<div class="p-flow01__image01">
				<img src="/assets/image/flow/img01.png" width="712" height="295" usemap="#Map" border="0" />
				<map name="Map" id="Map">
				  <area shape="poly" coords="51,107,51,192,125,192,125,107" href="#step01" />
				  <area shape="poly" coords="183,108,183,192,268,192,268,108" href="#step02" />
				  <area shape="poly" coords="329,111,329,187,392,187,393,111" href="#step03" />
				  <area shape="poly" coords="450,111,450,195,547,194,547,112" href="#step04" />
				  <area shape="poly" coords="598,114,597,185,674,187,672,116" href="#step05" />
				</map>
			</div>
			<div class="p-flow01__txt01">
				<p>
					 住宅にせよ、業務用の建物にせよ、初めての方が大半だと思います。<br>
					「何から始めて良いか？」わからないのは当然です。<br>
					そういった時に、最初に相談する窓口が我々「建築設計事務所」です。<br>
					一級建築士事務所SHEaPでは、建物のことだけでなく、<br>
					住まいづくりにおける資金計画や土地のこと、<br>
					事業用の土地や運営計画まで、<br>
					建築に関わる初期段階の相談を承っております。<br>
					気軽にお声かけください。
				</p>
				<p>下記に、住宅の場合を例に大まかなスケジュールを表示しましたので、ごらんください。 </p>
			</div>

			<div class="p-flow01__step">
				<div id="step01" class="p-flow01__step--item">
					<div class="p-flow01__steptitle">
						<p class="p-flow01__steptitle--img"><img src="/assets/image/flow/step01.png" width="48" height="68"></p>
						お気軽にご連絡ください。
					</div>
					<div class="p-flow01__step-cont01">
						<div class="step-img">
							<img src="/assets/image/flow/img02.png" width="275" height="187" alt="">
						</div>
						<div class="step-txt c-txt01">
							まずは、お気軽にご連絡ください。実際の暮らしを体感していただくことで、間取り、光や風の入り方、生活動線などの空間イメージや設備機器、建材などの素材のイメージの参考にしていただきやすいかと思います。<br>自分らしい住まいづくりのスタートです。
						</div>
					</div>
				</div>

				<div id="step02" class="p-flow01__step--item">
					<div class="p-flow01__steptitle">
						<p class="p-flow01__steptitle--img"><img src="/assets/image/flow/step02.png" width="48" height="68"></p>
						お客様の住まいへの想いをお聞かせください。
					</div>
					<div class="p-flow01__step-cont02">
						<div class="step-txt c-txt01">
							家族の顔が見える家、趣味を生かした家、ペットと暮らしやすい家、シンプルなデザインの家、窓の大きい家など、世界に一つしかないお客様の建てたい家のイメージや想いをたくさんお聞かせください。
						</div>
						<div class="step-img">
							<img src="/assets/image/flow/img03.png" width="242" height="190" alt="">
						</div>
					</div>
					<div class="p-flow__bg01"></div>
				</div>

				<div id="step03" class="p-flow01__step--item">
					<div class="p-flow01__steptitle">
						<p class="p-flow01__steptitle--img"><img src="/assets/image/flow/step03.png" width="48" height="68"></p>
						お客様の想いを図面上に形作っていきます。
					</div>
					<div class="p-flow01__step-cont01">
						<div class="step-img">
							<img src="/assets/image/flow/img04.png" width="235" height="189" alt="">
						</div>
						<div class="step-txt c-txt01">
							できあたってきたイメージをもと耐震・耐熱・防犯などの機能性も考慮しながら、アウトラインからでティールまでカタチを作っていきます。いよいよ、お客様個々の住まいが図面となり、平面上に絵となって浮かび上がります。
						</div>
					</div>
				</div>

				<div id="step04" class="p-flow01__step--item">
					<div class="p-flow01__steptitle">
						<p class="p-flow01__steptitle--img"><img src="/assets/image/flow/step04.png" width="48" height="68"></p>
						オンリーワンの住まいづくりが始まります。
					</div>
					<div class="p-flow01__step-cont02">
						<div class="step-txt c-txt01">
							近隣へと挨拶と鎮魂祭から始まります。<br>
							家の大きさにもよりますが、約3か月から5か月間、各業種のエキスパートが住まいづくりに関わります。各工程の記録、検査を経てオンリーワンの住まいが建ち上がります。
						</div>
						<div class="step-img">
							<img src="/assets/image/flow/img05.png" width="305" height="226" alt="">
						</div>
					</div>
				</div>


				<div id="step05" class="p-flow01__step--item">
					<div class="p-flow01__steptitle">
						<p class="p-flow01__steptitle--img"><img src="/assets/image/flow/step05.png" width="48" height="68"></p>
						お引き渡し・アフターフォロー
					</div>
					<div class="p-flow01__step-cont01">
						<div class="step-img">
							<img src="/assets/image/flow/img06.png" width="293" height="209" alt="">
						</div>
						<div class="step-txt c-txt01">
							完成後、竣工検査などを経て、晴れてお客様へとお引き渡しいたします。ついに、お客様だけの住まいの完成です。住み始めてからの不具合や、修繕、増築、改築なども、もちろんお気軽にご相談ください。<br>末長いお付き合いをお願いいたします。
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="p-flow02">
			<div class="p-flow__bg02"></div>
			<div class="p-flow02__paragraph">
				<div class="p-flow02__title">
					設計費について
				</div>
				<div class="c-txt01">
					<p>設計事務所の住まいづくりは、決して高くはありません。</p>

					<p>よく、「設計事務所に依頼すると、設計費が余分にかかるんじゃないの？」と言われますが、決してそうではありません。ハウスメーカー、工務店、大工さんに依頼する場合も、設計費という項目がない会社もありますが、必ず設計費はかかっているのです。</p>

					<p>設計事務所から工務店に依頼する場合、当然、設計費を抜いた金額で依頼する為、同じ建物を建てるならば、設計施工の会社に依頼する場合と比べて高くなるということはありません。</p>

					<p>もちろん、条件が難しかったり、特別なこだわりがある方の場合は、工事も含めそれなり費用になりますが、そういった場合が設計事務所としての腕の見せ所だと思っていますし、得意としてます。</p>
				</div>
			</div>

			<div class="p-flow02__paragraph">
				<div class="p-flow02__title">
					費用の目安
				</div>
				<div class="c-txt01">
					<p>
						計画建物の大きさや、敷地、申請、用途などの条件によってさまざまですが、<br>
						おおよそ、建築工事費の6％～10％です。<br>
						建物が大きいほど、スケールメリットがでて、設計費も安くなります。<br>
					</p>

					<p>30～40坪で、基本計画、実施設計、設計監理、外構計画、インテリアコーディネートなど、全て含んで、工事金額の8％くらいが目安となります。 </p>
				</div>
			</div>
		</div>

		<div class="c-contact">
			<div class="c-contact__infor">
				<p class="c-contact__infor--01">お問い合わせ・ご相談はお気軽に</p>
				<p class="c-contact__infor--02">090-5870-0000</p>
				<p class="c-contact__infor--03">受付時間　10：00〜20：00　◯曜定休</p>
			</div>
			<div class="c-contact__lable">
				<a href=""><img src="/assets/image/index/img16.png" width="140" height="140" alt=""></a>
			</div>
			<div class="c-bgtree">
				<img src="/assets/image/index/tree.png" width="557" height="625" alt="">
			</div>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>