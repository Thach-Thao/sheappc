<?php $id="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-top l-container-fluid">
	<div class="p-top01">
		<div class="l-wrapper">
			<div class="p-top01__title">
				スタイリッシュで<br>人と地球に優しい住まいづくり
			</div>

			<div class="p-top01__txt01">
				 豊かな住まいとは、ただ広く、豪華な素材を使うことではありません<br>
				本当の意味での「豊かな住まい」とは 住まう人に合っていることなのではないでしょうか<br>
				じっくりと話し、ライフスタイルにフィットしたものを見つけ出していく<br>
				一級建築士事務所 SHEaPは、そんな設計を心がけています
			</div>

			<div class="p-top01__img01">
				<img src="/assets/image/index/img01.png" width="162" height="216" alt="">
			</div>
		</div>

		<div class="p-top01__block">
			<div class="block-left">
				<img src="/assets/image/index/img02.png" width="348" height="486" alt="">
			</div>
			<div class="block-right">
				<div class="c-title02">
					こんにちは、<br>一級建築士事務所SHEaPの<br>戸塚治夫です。
				</div>
				<div class="c-txt01">
					<p>住まいづくりをする方が一番意識するのは、間取りや材料、デザイン、そして、コストとのバランスです。それを総合的に提案するのが設計者の役割だと考えています</p>
					<p>せっかくの住まいづくりですから、より住まう自分達にあったものを追求して欲しいと思います</p>
					<p>住まう方にフィットする住まいを提供する為に、お客様とのコミュニケーションを大切にして設計したいと思っています。</p>
				</div>
			</div>
		</div>
	</div>

	<div class="l-wrapper">
		<div class="p-top02">
			<div class="p-top02__left">
				<div class="c-title01">
					Des<span class="c-title01--color1">i</span>ng
					<span class="c-title01--smtxt c-title01--color1">6つの設計コンセプト</span>
				</div>
				<div class="c-title02">
					SHEaPでは、<br>
					6つの設計コンセプトを<br>
					大切にしています。
				</div>
				<div class="c-txt01">
					一級建築士事務所SHEaPでは、<br>
					「光」「風」「省エネ」「安心・安全」「広がり・つながり」「シンプル」といった6つの設計コンセプトを大切に考えています。
				</div>
			</div>
			<div class="p-top02__right">
				<img src="/assets/image/index/img03.png" width="354" height="457" alt="">
			</div>
		</div>

		<div class="p-top03">
			<div class="c-title01">
				G<span class="c-title01--color2">a</span>llery
				<span class="c-title01--smtxt c-title01--color2">設計事例と提案事例</span>
			</div>
			<div class="c-title02">
				SHEaPのお仕事の一部をご覧ください。
			</div>
			<div class="p-top03__cont">
				<div class="p-top03__cont--col1">
					<p class="img01"><img src="/assets/image/index/img05.png" width="157" height="233" alt=""></p>
					<p class="img02"><img src="/assets/image/index/img06.png" width="155" height="156" alt=""></p>
				</div>
				<div class="p-top03__cont--col2">
					<p><img src="/assets/image/index/img07.png" width="153" height="202" alt=""></p>
					<p><img src="/assets/image/index/img08.png" width="233" height="155" alt=""></p>
				</div>
				<div class="p-top03__cont--col3">
					<p><img src="/assets/image/index/img09.png" width="156" height="233" alt=""></p>
					<p class="bg"><img src="/assets/image/index/img17.png" width="116" height="134" alt=""></p>
				</div>
			</div>
		</div>

		<div class="p-top04">
			<div class="c-title01">
				Sched<span class="c-title01--color1">u</span>le
				<span class="c-title01--smtxt c-title01--color1">住まいづくりのすすめ方</span>
			</div>
			<div class="c-title02">
				住まいづくりの流れをご紹介しています。
			</div>
			<div class="c-txt01">
				この限りではありませんので、ご要望に合わせてご相談ください。
			</div>
			<div class="p-top04__block">
				<div class="p-top04__item">
					<div class="block-img">
						<p><img src="/assets/image/index/img10.png" width="255" height="233" alt=""></p>
					</div>
					<div class="block-text">
						<div class="c-title03">
							<span>1.</span>聞く<span class="c-title03--smtxt">基本計画段階 1.5〜2ヶ月</span>
						</div>
						<div class="c-line c-line--01"></div>
						<div class="c-txt01">
							<span>お引き合い・打ち合せ</span><br>
							・現在の悩みと「こうなりたい」のイメージ<br>
							・敷地の調査<br>
							・法規チェック
						</div>
						<div class="c-line c-line--02"></div>
						<div class="c-txt01">
							<span>お引き合い・打ち合せ</span><br>
							・現在の悩みと「こうなりたい」のイメージ<br>
							・敷地の調査<br>
							・法規チェック
						</div>
					</div>
				</div>

				<div class="p-top04__item">
					<div class="block-img">
						<p><img src="/assets/image/index/img11.png" width="255" height="233" alt=""></p>
					</div>
					<div class="block-text">
						<div class="c-title03">
							<span>2.</span>考える<span class="c-title03--smtxt">設計段階 3〜6ヶ月</span>
						</div>
						<div class="c-line c-line--01"></div>
						<div class="c-txt01">
							<span>お引き合い・打ち合せ</span><br>
							・現在の悩みと「こうなりたい」のイメージ<br>
							・敷地の調査<br>
							・法規チェック
						</div>
						<div class="c-line c-line--02"></div>
						<div class="c-txt01">
							<span>プレゼンテーション</span><br>
							・基本計画図（配置図・平面図・立面図）<br>
							・イメージスケッチ等<br>
							・設計業務 概算のお見積もり
						</div>
					</div>
				</div>

				<div class="p-top04__item">
					<div class="block-img">
						<p><img src="/assets/image/index/img12.png" width="255" height="233" alt=""></p>
					</div>
					<div class="block-text">
						<div class="c-title03">
							<span>3.</span>つくる<span class="c-title03--smtxt">監理段階 監理7〜8ヶ月</span>
						</div>
						<div class="c-line c-line--01"></div>
						<div class="c-txt01">
							<span>お引き合い・打ち合せ</span><br>
							・現在の悩みと「こうなりたい」のイメージ<br>
							・敷地の調査<br>
							・法規チェック
						</div>
						<div class="c-line c-line--02"></div>
						<div class="c-txt01">
							<span>プレゼンテーション</span><br>
							・基本計画図（配置図・平面図・立面図）<br>
							・イメージスケッチ等<br>
							・設計業務 概算のお見積もり
						</div>
					</div>
				</div>

				<div class="p-top04__item">
					<div class="block-img">
						<p><img src="/assets/image/index/img13.png" width="255" height="233" alt=""></p>
					</div>
					<div class="block-text">
						<div class="c-title03">
							<span>4.</span>住まう<span class="c-title03--smtxt">お引渡し</span>
						</div>
						<div class="c-line c-line--01"></div>
						<div class="c-txt01">
							<span>お引き合い・打ち合せ</span><br>
							・現在の悩みと「こうなりたい」のイメージ<br>
							・敷地の調査<br>
							・法規チェック
						</div>
						<div class="c-line c-line--02"></div>
						<div class="c-txt01">
							<span>プレゼンテーション</span><br>
							・基本計画図（配置図・平面図・立面図）<br>
							・イメージスケッチ等<br>
							・設計業務 概算のお見積もり
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="p-top05">
			<div class="p-top05__title">
				<p class="p-top05__title--01">BLOG</p>
				<p class="p-top05__title--02">ヒツジ的住まいズム！</p>
			</div>
			<div class="p-top05__img">
				<img src="/assets/image/index/img14.png" width="116" height="135" alt="">
			</div>
			<div class="p-top05__cont">
				<div class="p-top05__cont--txt">タイトル</div>
				<div class="c-txt01">
					<p>記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。</p>

					<p>記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。記事内容がここに表示されます。<br>
					記事内容がここに表示されます。記事内容がここに表示されます。</p>
				</div>
			</div>
		</div>

		<div class="p-top06">
			<div class="c-title01">
				Q<span class="c-title01--color1">&</span>A
				<span class="c-title01--smtxt c-title01--color1">よく寄せられる質問</span>
			</div>
			<div class="p-top06__img">
				<img src="/assets/image/index/img15.png" width="122" height="141" alt="">
			</div>

			<div class="p-top06__cont">
				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q1.</p>
						<p class="box__txt--02">設計事務所って<br>高いんじゃないの？</p>
					</div>
				</div>

				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q2.</p>
						<p class="box__txt--02">工事は請け負わないの？</p>
					</div>
				</div>

				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q3.</p>
						<p class="box__txt--02">小さな会社に依頼しても<br>安心できますか？</p>
					</div>
				</div>

				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q4.</p>
						<p class="box__txt--02">設計事務所って<br>高いんじゃないの？</p>
					</div>
				</div>

				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q5.</p>
						<p class="box__txt--02">工事は請け負わないの？</p>
					</div>
				</div>

				<div class="box">
					<div class="box__img"></div>
					<div class="box__txt">
						<p class="box__txt--01">Q6.</p>
						<p class="box__txt--02">小さな会社に依頼しても<br>安心できますか？</p>
					</div>
				</div>
			</div>
		</div>

		<div class="p-top07">
			<div class="c-title01">
				Contact
				<span class="c-title01--smtxt c-title01--color1">お問い合わせ</span>
			</div>
			<div class="c-title02">
				住まいづくり・建築計画に係る相談を承っております。<br>相談は無料です。気軽にお声かけください。 
			</div>

			<form  class="form-contact" method="">
				<div class="form-lable">
					お名前<span>【必須】</span>
				</div>
				<input class="form-text" type="text" name="">

				<div class="form-lable">
					Email<span>【必須】</span>
				</div>
				<input class="form-text" type="text" name="">

				<div class="form-lable">お問い合わせ種別</div>

				<input style="margin-left:0;" class="form-check" type="checkbox" name="" value="">新築
  				<input class="form-check" type="checkbox" name="" value="">リフォーム
  				<input class="form-check" type="checkbox" name="" value="">アパート・マンションオーナー様
  				<input class="form-check" type="checkbox" name="" value="">相談会申込み
				<input class="form-check" type="checkbox" name="" value="">その他

				<div class="form-lable">郵便番号	</div>
				<input class="form-text" type="text" name="">

				<div class="form-lable">ご住所</div>
				<input class="form-text" type="text" name="">

				<div class="form-lable">お電話番号</div>
				<input class="form-text" type="text" name="">

				<div class="form-lable">
					お名前<span>【必須】</span>
				</div>
				<textarea class="form-textarea" type="text" name=""></textarea>

				<div class="form-btn">
					<a href="#">リセット</a>
					<a href="#">確認画面へ</a>
				</div>
			</form>

			<div class="c-contact">
				<div class="c-contact__infor">
					<p class="c-contact__infor--01">お問い合わせ・ご相談はお気軽に</p>
					<p class="c-contact__infor--02">090-5870-0000</p>
					<p class="c-contact__infor--03">受付時間　10：00〜20：00　◯曜定休</p>
				</div>
				<div class="c-contact__lable">
					<a href=""><img src="/assets/image/index/img16.png" width="140" height="140" alt=""></a>
				</div>
				<div class="c-bgtree">
					<img src="/assets/image/index/tree.png" width="557" height="625" alt="">
				</div>
			</div>
		</div>
	</div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>