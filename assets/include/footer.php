<footer class="c-footer">
	<div class="c-footer01">
		<div class="l-container">
			<div class="c-footer01__menu">
				<div class="c-footer01__txt">
					<a href=""><img src="/assets/image/common/icon-home.png" width="21" height="20" alt="" > ホーム</a>
				</div>
				<div class="c-footer01__col">
					<div class="c-footer01__block">
						<div class="menu-title">
							<span>About</span>シープについて
						</div>
						<ul>
							<li><a href="#">設計事務所と創りましょう</a></li>
							<li><a href="#">事務所概要</a></li>
							<li><a href="#">プロフィール</a></li>
							<li><a href="#">アクセスマップ</a></li>
						</ul>
					</div>

					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Gallery</span>ギャラリー
						</div>
					</div>

					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Flow</span>住まいづくりのすすめ方
						</div>
					</div>
				</div>

				<div class="c-footer01__col">
					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Desing</span>デザインコンセプト
						</div>
						<ul>
							<li><a href="#">光</a></li>
							<li><a href="#">風</a></li>
							<li><a href="#">省エネ</a></li>
							<li><a href="#">安心・安全</a></li>
							<li><a href="#">広がり・つながり</a></li>
							<li><a href="#">シンプル</a></li>
						</ul>
					</div>

					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Contact</span>お問い合わせ
						</div>
					</div>
				</div>

				<div class="c-footer01__col">
					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Q&A</span>質問と解答
						</div>
					</div>
					<div class="c-footer01__block">
						<div class="menu-title">
							<span>Blog</span>ブログ
						</div>
					</div>
					<div class="c-txt01">
						プライバシーポリシー<br>
						サイトマップ
					</div>
				</div>


			</div>
			<div class="c-footer01__social">
				<div class="c-footer01__txt"><a href="">ページトップへ</a></div>
				<div class="c-footer01__social--item"><a href="">
					<div class="social-icon">
						<img src="/assets/image/common/icon01.png" width="29" height="29"alt="">
					</div>
					<div class="social-cont">
						公式Facebookはこちら
					</div>
				</a></div>

				<div class="c-footer01__social--item"><a href="">
					<div class="social-icon">
						<img src="/assets/image/common/icon02.png" width="28" height="28"alt="">
					</div>
					<div class="social-cont">
						ひつじ的住まいズム！
					</div>
				</a></div>

				<div class="c-footer01__social--item"><a href="">
					<div class="social-icon">
						<img src="/assets/image/common/icon02.png" width="29" height="29"alt="">
					</div>
					<div class="social-cont">
						ヒツジ的フォトライフ！
					</div>
				</a></div>
			</div>
		</div>
	</div>
	<div class="l-container">
		<div class="c-footer02">
			<div class="c-footer02__logo">
				<a href="/"><img src="/assets/image/common/f-logo.png" width="147" height="61"  alt=""></a>
			</div>
			<div class="c-footer02__infor">
				<p class="c-footer02__infor--txt">一級建築士事務所登録　静岡県知事登録（◯-00）第00000号<br>
				〒400-0000静岡県浜松市中区三組町28-76<br>
				TEL：053-000-0000 &nbsp; &nbsp; FAX：053-000-0000</p>
				<p class="c-footer02__infor--icon">
					<a href="#"><img src="/assets/image/common/iconfb.png" width="73" height="23" alt=""></a>
				</p>
			</div>
		</div>
	</div>
	<div class="c-footer03">
		<div class="l-container">
			<p class="c-footer03__copyright">Copyright © 2015 SHEaP inc. All Rights Reserved.</p>
		</div>
	</div>
</footer>


<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="/assets/js/slick/slick-theme.css">

	<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
	<script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script src="/assets/js/slick/slick.js"></script>

	<script src="/assets/js/functions.js"></script>
	<script type="text/javascript">

		//slider
		$(document).ready(function(){
			$('.c-header .slider').slick({
				dots: true,
				autoplay: true,
				autoplaySpeed: 3000,
			});
		});

		//accodition page Q&A

		var acc = document.getElementsByClassName("p-accodition__button");
		var i;

		for (i = 0; i < acc.length; i++) {
		 	acc[i].onclick = function() {
		    	var active = document.querySelector(".p-accodition-active");
		    	if (active && active != this) {
		      		active.classList.remove("p-accodition-active");
		      		active.nextElementSibling.classList.remove("p-acodition-show");
		    	}
		   		this.classList.toggle("p-accodition-active");
		    	this.nextElementSibling.classList.toggle("p-acodition-show");
		  	}
		}

	</script>
</body>
</html>