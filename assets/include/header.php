<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/assets/js/jquery.matchHeight-min.js"></script>

</head>
<body class="page-<?php echo $id; ?>">


<!------------------- PAGE TOP ---------------------->
<?php
	if(isset($id)){
		if($id=="top"){
?>
<header class="l-container-fluid">
<div class="c-header">
	<div class="c-header__bg">
		<img src="/assets/image/common/h_bg.png" width="1495" height="701" alt="">
	</div>

	<div class="c-header__content">
		<div class="c-header__text">
			<p><img src="/assets/image/common/h_txt.png" width="459" height="24" alt=""></p>
			<p class="c-header__text--contact">
				<a href="#"><img src="/assets/image/common/h_contact.png" width="107" height="107" alt=""></a>
			</p>
		</div>
		<div class="c-h_infor">
			<div class="tbl-infor">
				<div class="tbl-infor__txt01">
					一級建築士事務所 <span>シープ</span>
				</div>
				<div class="tbl-infor__logo">
					<a href="#"><img src="/assets/image/common/h_logo.png" width="128" height="53" alt=""></a>
				</div>
				<div class="tbl-infor__txt02">
					日々をたのしむ住まい。
				</div>
				<div class="tbl-infor__txt03">
					Q&A<br>
					ブログ<br>
					住まいづくりのすすめ方<br>
					ギャラリー<br>
					デザインコンセプト<br>
					シープについて
				</div>
				<div class="tbl-infor__img">
					<img src="/assets/image/common/h_img01.png" width="76" height="50" alt="">
				</div>
				<div class="tbl-infor__txt04">
					<p>サイトマップ</p>
					<p>プライバシーポリシー</p>
				</div>
			</div>
			<div class="c-h_infor--home">
				<a href="/"><img src="/assets/image/common/btn_home.png" width="58" height="65" alt=""></a>
			</div>
		</div>
		<div class="c-header__slide">
			<div class="slide">
				<div class="slider">
					<div>
						<img src="/assets/image/common/slide01.png" width="1317" height="519" alt="">
					</div>
					<div>
						<img src="/assets/image/common/slide01.png" width="1317" height="519" alt="">
					</div>
					<div>
						<img src="/assets/image/common/slide01.png" width="1317" height="519" alt="">
					</div>
					<div>
						<img src="/assets/image/common/slide01.png" width="1317" height="519" alt="">
					</div>
					<div>
						<img src="/assets/image/common/slide01.png" width="1317" height="519" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</header>
<?php } else{ ?>

<!----------------------------------------------------->

<header class="c-head l-container-fluid">
<div class="c-head01">
	<div class="c-h_infor">
		<div class="tbl-infor">
			<div class="tbl-infor__txt01">
				一級建築士事務所 <span>シープ</span>
			</div>
			<div class="tbl-infor__logo">
				<a href="#"><img src="/assets/image/common/h_logo.png" width="128" height="53" alt=""></a>
			</div>
			<div class="tbl-infor__txt02">
				日々をたのしむ住まい。
			</div>
			<div class="tbl-infor__txt03">
				Q&A<br>
				ブログ<br>
				住まいづくりのすすめ方<br>
				ギャラリー<br>
				デザインコンセプト<br>
				シープについて
			</div>
			<div class="tbl-infor__img">
				<img src="/assets/image/common/h_img01.png" width="76" height="50" alt="">
			</div>
			<div class="tbl-infor__txt04">
				<p>サイトマップ</p>
				<p>プライバシーポリシー</p>
			</div>
		</div>
		<div class="c-h_infor--home">
			<a href="/"><img src="/assets/image/common/btn_home.png" width="58" height="65" alt=""></a>
		</div>
	</div>
</div>
<div class="c-head02">
	<a href="#"><img src="/assets/image/common/h_contact.png" width="107" height="107" alt=""></a>
</div>
<div class="c-head__bg">
	<img src="/assets/image/common/h_bg02.png" width="493" height="466" alt="">
</div>
</header>

<?php }} ?>